<?php
namespace SymfonyPhpUnitCase\Tests;

use Psr\Log\LoggerInterface;
use SymfonyPhpUnitCase\Cases\ServiceTestCase;

class ServiceCaseTest extends ServiceTestCase {

	public function servicesToUnprivate(): array {
		return [
			'logger',
		];
	}

	public function testLogger() {
		$loggerService = $this->getService('logger');

		$this->assertInstanceOf(LoggerInterface::class, $loggerService);
	}
}