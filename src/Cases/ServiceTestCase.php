<?php
namespace SymfonyPhpUnitCase\Cases;

class ServiceTestCase extends KernelTestCase {

	public function bundleClassesToRegister(): array {
		return [];
	}

	public function servicesToUnprivate(): array {
		return [];
	}

	public function baseParameters(): array {
		return [];
	}

	protected function getService(string $serviceName) {
		return static::$kernel->getContainer()->get($serviceName);
	}
}