<?php
namespace SymfonyPhpUnitCase\Cases;

use Doctrine\Bundle\DoctrineBundle\DoctrineBundle;
use Doctrine\Common\Annotations\AnnotationRegistry;

class DataBaseTestCase extends ServiceTestCase {
	/**
	 * @var \Doctrine\ORM\EntityManager
	 */
	protected $entityManager;

	protected const VENDOR_PATH = __DIR__.'/../../../..';

	public static function setUpBeforeClass() {
		$loader = require self::VENDOR_PATH.'/autoload.php';
		AnnotationRegistry::reset();
		AnnotationRegistry::registerLoader([$loader, 'loadClass']);
	}

	protected function setUp() {
		parent::setUp();

		$this->entityManager = $this->getService('doctrine.orm.default_entity_manager');
		$this->entityManager->beginTransaction();
	}

	protected function tearDown() {
		$this->entityManager && $this->entityManager->rollback();
	}

	public function baseParameters(): array {
		return [
			'database_host'     => getenv('database_host') ?: 'localhost',
			'database_database' => getenv('database_name') ?: 'some-db-name',
			'database_username' => getenv('database_user') ?: 'root',
			'database_password' => getenv('database_password') ?: ''

		];
	}

	public function servicesToUnprivate(): array {
		return [
			'doctrine.orm.default_entity_manager'
		];
	}

	public function bundleClassesToRegister(): array {
		return [
			DoctrineBundle::class
		];
	}
}